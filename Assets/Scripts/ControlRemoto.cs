﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uriarte_Quijandria_Alonso_Omar_Examen01
{
    class ControlRemoto : Juguete
    {
        public int rango;
        public bool pilas;

        public ControlRemoto(int codigo, string marca, bool pilas, int rango) : base(codigo, marca)
        {
            this.rango = rango;
            this.pilas = pilas;
        }
    }
}
