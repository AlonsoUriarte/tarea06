﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uriarte_Quijandria_Alonso_Omar_Examen01
{
    class Juguete
    {
        public int codigo;
        public string marca;

        protected Juguete(int codigo, string marca)
        {
            this.codigo = codigo;
            this.marca = marca;
        }
        public void Encender()
        {
            Console.WriteLine("Jueguete encendido");
        }
        
    }
}
