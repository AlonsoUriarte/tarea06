﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;
using UnityEngine.UI;

public class Canvas : MonoBehaviour
{
    public GameObject TextBox;
    public GameObject Opción1;
    public GameObject Opción2;
    bool OpcionElegida;

    public void Opcion1Elegida () 
    {
        TextBox.GetComponent<Text>().text = "-Drone \nCodigo: 768165, marca Parrot. No tiene pilas y posee un rango de control de 20 metros. \nJueguete encendido \n-Carro \nCodigo:224067, marca Hot Wheels.No tiene pilas y posee un rango de control de 6 metros. \nJueguete encendido \n-Helicoptero \nCodigo:660379, marca CopterX. No tiene pilas y posee un rango de control de 15 metros. \nJueguete encendido";
    }
    public void Opcion2Elegida () 
    {
        TextBox.GetComponent<Text>().text = "-Perro Robot \nCodigo: 927320, marca Aibo. Puede realizar 5 acciones. \nJueguete encendido";
    }
}
