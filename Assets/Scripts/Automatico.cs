﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uriarte_Quijandria_Alonso_Omar_Examen01
{
    class Automatico : Juguete
    {
        public int acciones;

        public Automatico(int codigo, string marca, int acciones) : base(codigo, marca) 
        {
            this.acciones = acciones;
        }

        public void RealizarAccion()
        {
            Console.WriteLine("Baila");
        }
        
    }
}
