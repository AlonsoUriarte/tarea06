﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uriarte_Quijandria_Alonso_Omar_Examen01
{
    class Programa
    {
        static void Main(string[] args)
        {


                Console.WriteLine("");
                Console.WriteLine("Selecciona una opcion escribiendo su numero");
                Console.WriteLine("1-Juguetes a control remoto");
                Console.WriteLine("2-Juguetes automaticos");
                Console.WriteLine("3-Salir");

                string str = Console.ReadLine();



                switch (str)
                
                {
                    case "1":
                    Console.Clear();
                    
                    ControlRemoto controlRemoto01 = new ControlRemoto(768165, "Parrot", false, 20);
                    Console.WriteLine("Drone");
                    Console.WriteLine("Codigo:"+controlRemoto01.codigo+", marca "+controlRemoto01.marca+". No tiene pilas y posee un rango de control de "+controlRemoto01.rango+" metros.");
                    controlRemoto01.Encender();
            
                    Console.WriteLine("");
                    ControlRemoto controlRemoto02 = new ControlRemoto(224067, "Hot Wheels", true, 6);
                    Console.WriteLine("Carro");
                    Console.WriteLine("Codigo:" + controlRemoto02.codigo + ", marca " + controlRemoto02.marca + ". No tiene pilas y posee un rango de control de " + controlRemoto02.rango + " metros.");
                    controlRemoto02.Encender();

                    Console.WriteLine("");
                    ControlRemoto controlRemoto03 = new ControlRemoto(660379, "CopterX", true, 15);
                    Console.WriteLine("Helicoptero");
                    Console.WriteLine("Codigo:" + controlRemoto03.codigo + ", marca " + controlRemoto03.marca + ". No tiene pilas y posee un rango de control de " + controlRemoto03.rango + " metros.");
                    controlRemoto03.Encender();

                    break;

                    case "2":
                    Console.Clear();

                    Console.WriteLine("");
                    Automatico automatico = new Automatico(927320, "Aibo", 5);
                    Console.WriteLine("Perro Robot");
                    Console.WriteLine("Codigo:" + automatico.codigo + ", marca " + automatico.marca + ". Puede realizar "+automatico.acciones+" acciones.");
                    automatico.Encender();
                    automatico.RealizarAccion();

                    break;

                    case "3":
                    Console.Clear();


                    break;

                    default:
                    Console.Clear();
                    Console.WriteLine("Opcion no valida");
                    break;
                }

      
           

        }
    }
}
